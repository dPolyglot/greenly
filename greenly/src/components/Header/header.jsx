import React from 'react';
import './header.css';
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


function Header(){
    return(
        <div className="flexed">
            <h3>Greenly</h3>
            <FontAwesomeIcon icon={faUser} />
        </div>
    )
}

export default Header;