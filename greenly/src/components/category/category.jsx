import React from 'react';
import './category.css'

function Categorycard(props){
    return(
        <div className="category-card">
            <p style={{ marginBottom: 5 + 'px'}}>{props.title}</p>
            <p style={{
                fontSize: 10 + 'px'
            }}>{props.drugs}</p>
            
        </div>
    )
}

export default Categorycard;