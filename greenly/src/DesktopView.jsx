import React from 'react';

function DesktopView(){
    
    return(
        <h1 className="desktopView">This app is not available for desktop. View on mobile</h1>
    )
}

export default DesktopView;