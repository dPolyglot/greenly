import React, {Component} from 'react';
import './App.css';
import Header from './components/Header/header';
import Categorycard from './components/category/category';
import DesktopView from './DesktopView';



class App extends Component{

  state = {
    categoryData: [
      {name: 'Malaria', drugs: '200', id: 1},
      {name: 'Cough', drugs: '50', id: 2},
      {name: 'High Blood Pressure', drugs: '100', id: 3}
    ]
  }

  
  render(){

    let categories = (
      <div className="category-box">
        {this.state.categoryData.map(allCategory => {
          return <Categorycard key={allCategory.id} title={allCategory.name} drugs={allCategory.drugs + ' drugs available'}>
            <button>New</button>
          </Categorycard>

          
        })}      
    </div>
    )
    
    return (
      <div className="App">
        <div className="App-box">
          <DesktopView/>
          
          <div className="viewMobile">
          
            <Header/>            
            <div className="belowHeader">
              <h1 className="title">What are you looking for?</h1>
              <input className="searchbar" type="text" placeholder="Search for drugs"/>
              {categories}
            </div>
    
          </div>
        </div>
      </div>
    );
  }
}

export default App;
