const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan')

const connectDB = require('./config/db')

// Load env vars
dotenv.config({ path: './config/config.env' });
connectDB()

// router files
const drugCategories = require('./routes/drug-categories')
const drugs = require('./routes/drugs')
const app = express();

if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'))
}

// Mount routers
app.use('/api/v1/drug-categories', drugCategories)
app.use('/api/v1/drugs', drugs)

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`))

process.on('unhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`)
    // server.close(() => process.exit(1))
})
