const mongoose = require('mongoose');

const DrugSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Drug name is required'],
        unique: true,
        trim: true
    },
    description: {
        type: String,
        required: [true, 'Description is required'],
        maxlength: [500, 'Characters should not exceed 500']
    },
    price: {
        type: String,
        required: [true, 'Drug price is required']
    },
    dosage: {
        type: String,
        required: [true, 'Drug dosage is required']
    },
    age: {
        type: String,
        required: [true, 'Age for dosage is required']
    },
    photo: {
        type: String,
        default: 'no-photo.jpg'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Drugs', DrugSchema)