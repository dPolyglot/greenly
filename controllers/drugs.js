// @desc        Get all drugs
// @route       GET /api/v1/drugs
// @access      Public
exports.drugs = (req, res,next) => {
    res.status(200).json({success: true, drugs: 'All drugs on Greenly'})
}

// @desc        Get a drug
// @route       GET /api/v1/drugs/:id
// @access      Public
exports.viewDrug = (req, res,next) => {
    res.status(200).json({success: true, drugs: `View drug ${req.params.id}`})
}

// @desc        Create new drug
// @route       POST /api/v1/drugs
// @access      Private
exports.createDrug = (req, res,next) => {
    res.status(200).json({success: true, drugs: 'Create new drug'})
}

// @desc        Update a drug
// @route       PUT /api/v1/drugs/:id
// @access      Private
exports.updateDrug = (req, res,next) => {
    res.status(200).json({success: true, drugs: `Update drug ${req.params.id}`})
}

// @desc        Delete a drug
// @route       DELETE /api/v1/drugs/:id
// @access      Private
exports.deleteDrug = (req, res,next) => {
    res.status(200).json({success: true, drugs:`Delete drug ${req.params.id}`})
}