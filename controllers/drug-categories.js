const Categories = require('../models/Categories')
// @desc        Get all drug categories
// @route       GET /api/v1/drug-categories
// @access      Public
exports.drugCategories = (req, res,next) => {
    res.status(200).json({success: true, drugCategories: 'All drug categories'})
}

// @desc        Get a drug category
// @route       GET /api/v1/drug-categories/:id
// @access      Public
exports.viewDrugCategory = (req, res,next) => {
    res.status(200).json({success: true, drugCategories: `View drug category ${req.params.id}`})
}

// @desc        Get all drug categories
// @route       POST /api/v1/drug-categories
// @access      Private
exports.createDrugCategory = (req, res,next) => {
    res.status(200).json({success: true, drugCategories: 'Create new drug category'})
}

// @desc        Update drug category
// @route       PUT /api/v1/drug-categories/:id
// @access      Private
exports.updateDrugCategory = (req, res,next) => {
    res.status(200).json({success: true, drugCategories: `Update drug category ${req.params.id}`})
}

// @desc         Delete drug category
// @route       DELETE /api/v1/drug-categories/:id
// @access      Private
exports.deleteDrugCategory = (req, res,next) => {
    res.status(200).json({success: true, drugCategories: `Delete drug ${req.params.id}`})
}