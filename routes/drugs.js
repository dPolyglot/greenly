const express = require('express');
const {
    drugs,
    viewDrug,
    createDrug,
    updateDrug,
    deleteDrug
} = require('../controllers/drugs')
const router = express.Router();

router
    .route('/')
    .get(drugs)
    .post(createDrug)

router
    .route('/:id')
    .get(viewDrug)
    .put(updateDrug)
    .delete(deleteDrug)
    
module.exports = router