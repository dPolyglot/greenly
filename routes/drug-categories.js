const express = require('express');
const {
    drugCategories,
    viewDrugCategory,
    createDrugCategory,
    updateDrugCategory,
    deleteDrugCategory
} = require('../controllers/drug-categories');

const router = express.Router();

router
    .route('/')
    .get(drugCategories)
    .post(createDrugCategory)

router
    .route('/:id')
    .get(viewDrugCategory)
    .put(updateDrugCategory)
    .delete(deleteDrugCategory)

module.exports = router;
